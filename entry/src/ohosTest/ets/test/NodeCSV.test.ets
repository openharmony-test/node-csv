/**
 *  MIT License
 *
 *  Copyright (c) 2023 Huawei Device Co., Ltd.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

import { generate,Generator,Options} from 'csv-generate/browser/esm'
import { stringify ,Stringifier} from 'csv-stringify/browser/esm'
import { transform,Transformer } from 'stream-transform/browser/esm'

import { describe, expect, it } from '@ohos/hypium'

export default function jodaNodeCSVTest() {
  let result:string = "earth,China,jiangsu"
  let resultGenerate:string = '[["OMH","ONKCHhJmjadoA"],["D","GeACHiN"]]'
  let text1:string = "earth China jiangsu"
  let text2:string = ""
  let input: ESObject[] = []
  let index: number = 0;

  describe('csvTest', () => {

    it('assertLocalDate_stringify', 0, () => {
      input.push(text1.split(" "))
      let outPut:Stringifier = stringify(input, (err, output) => {
      });
      expect(outPut != null).assertTrue()
    })

    it('assertLocalDate_stringify1', 0, () => {
      input.push(text1.split(" "))
      let stringifyReturn: Stringifier = stringify(input,
        {
          bom:true,
          quoted_string:true,
          escape_formulas:true,
          quoted:true
        },
        (err, output) => {});
      expect(stringifyReturn != null).assertTrue()
    })


    it('assertLocalDate_stringify2', 0, () => {
      input.push(text1.split(" "))
      let stringifyReturn: Stringifier = stringify(input,
        {
          bom:true,
          quoted_string:true,
          escape_formulas:true,
          quoted:true
        },
        (err, output) => {
        });
      expect(stringifyReturn != null).assertTrue()
    })

    it('assertLocalDate_stringify222', 0, () => {
      input.push(text1.split(" "))
      let stringifyReturn: Stringifier = stringify(input,
        {
          bom:true,
          quoted_string:true,
          escape_formulas:true,
          quoted:true
        },
        (err, output) => {});
      expect(stringifyReturn != null).assertTrue()
    })

    it('assertLocalDate_stringify3', 0, () => {
      input.push(text1.split(" "))
      let stringifyReturn : Stringifier = stringify(input,
        {
          bom:true,
          quoted_string:true,
          escape_formulas:true,
          quoted:true
        },
        (err, output) => {});
      expect(stringifyReturn != null).assertTrue()
    })

    it('assertLocalDate_generate1', 0, () => {
      let generateReturn : Generator = generate({
        seed: 2,
        objectMode: true,
        columns: 2,
        length: 2
      }, (err, records:ESObject) => {});
      expect(generateReturn != null).assertTrue()
    })

    it('assertLocalDate_generate2', 0, () => {
      let generateReturn : Generator = generate({
        seed: 1,
        objectMode: true,
        columns: 2,
        length: 2
      }, (err, records:ESObject) => {});
      expect(generateReturn != null).assertTrue()
    })

    it('assertLocalDate_generate3', 0, () => {
      let generator:Generator = generate({
        seed: 1,
        objectMode: true,
        columns: 2,
        length: 2
      }, (err, records:ESObject) => {});
      expect(generator.options.seed == 1).assertTrue()

    })

    it('assertLocalDate_generate4', 0, () => {
      let generator:Generator = generate({
        seed: 1,
        objectMode: true,
        columns: 2,
        length: 2
      }, (err, records:ESObject) => {});
      expect(generator.options.objectMode).assertTrue()
    })

    it('assertLocalDate_generate5', 0, () => {
      let generator:Generator = generate({
        seed: 1,
        objectMode: true,
        columns: 2,
        length: 2
      }, (err, records:ESObject) => {});
      expect(generator.options.length == 2).assertTrue()
    })

    it('assertLocalDate_generate6', 0, () => {
      let generator:Generator = generate({
        seed: 1,
        objectMode: true,
        duration:5,
        columns: 2,
        length: 2
      }, (err, records:ESObject) => {});
      expect(generator.options.duration == 5).assertTrue()
    })

    it('assertLocalDate_generate7', 0, () => {
      let generator:Generator = generate({
        seed: 2,
        objectMode: true,
        columns: 2,
        length: 2
      }, (err, records:ESObject) => {});
      expect(generator.options.objectMode).assertTrue()
    })

    it('assertLocalDate_generate8', 0, () => {
      let generator2 :Generator = generate({
        seed: 2,
        objectMode: true,
        columns: 2,
        length: 2
      }, (err, records:ESObject) => {
      });
      expect(generator2.options.seed == 2).assertTrue()
    })

    it('assertLocalDate_transform', 0, () => {
      let transformReturn : Transformer = transform([['1', '2', '3', '4','5'], ['a', 'b', 'c', 'd']],{
        consume: true,
        parallel: 2,
      }, (record) => {}, (err, output) => {});
      expect(transformReturn.options.consume).assertTrue()
    })

    it('assertLocalDate_transform1', 0, () => {
      let transformReturn : Transformer = transform([['1', '2'], ['a', 'b']], {
        consume: true,
        parallel: 2,
      },(record) => {}, (err, output) => {});
      expect(transformReturn.options.parallel == 2).assertTrue()
    })

    it('assertLocalDate_transform2', 0, () => {
      let transformReturn : Transformer = transform([['1', '2']], (record) => {}, (err, output) => {});
      expect(transformReturn != null).assertTrue()
    })


    it('assertLocalDate_transform3', 0, () => {
      let transformReturn:Transformer = transform([['1', '2']],
        {
          consume:false,
          parallel:5,
          params:"666"
        },
        (record) => {}, (err, output) => {});
      expect(!transformReturn.options.consume).assertTrue()
    })

    it('assertLocalDate_transform4', 0, () => {
      let textTransform = ""
      let transformReturn:Transformer = transform([['1', '2']], {
        consume:true,
        parallel:5,
        params:"666"
      },
        (record) => {}, (err, output) => {});
      expect(transformReturn.options.parallel === 5).assertTrue()
    })

    it('assertLocalDate_transform5', 0, () => {
      let transformReturn:Transformer = transform([['1', '2']],{
        consume:true,
        parallel:5,
        params:"666"
      }, (record) => {}, (err, output) => {});
      expect(transformReturn.options.params === "666").assertTrue()
    })
  })
}